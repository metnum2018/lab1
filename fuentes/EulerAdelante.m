function y = EulerAdelante(u0, v0, p0, q0, h, tol, maxIter)

	y(1,1) = u0;
	y(1,2) = v0;
	y(1,3) = p0;
	y(1,4) = q0;

	i = 1;
	do

		i++;
		u = y(i-1,1);
		v = y(i-1,2);
		p = y(i-1,3);
		q = y(i-1,4);

		y(i,1) = u + h * p;
		y(i,2) = v + h * q;
		y(i,3) = p + h * ( ((-2 * v) / (u^2 + v^2 + 1)) * p * q );
		y(i,4) = q + h * ( ((-2 * u) / (u^2 + v^2 + 1)) * p * q );

	until (i > maxIter || (abs(y(i,1) - u) <= tol && abs(y(i,2) - v) <= tol && abs(y(i,3) - p) <= tol && abs(y(i,4) - q) <= tol))

	y = y(2:length(y), :);
endfunction
