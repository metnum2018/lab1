function Main(x0, h, tol, maxIter)
	euler = EulerAdelante(x0(1), x0(2), x0(3), x0(4), h, tol, maxIter);
	for i=1:length(euler)
		x(i) = euler(i,1) * euler(i,2);
	end
	
	native = lsode("DiffEc", x0, t = linspace(0, length(euler) * h, length(euler)));
	for i=1:length(native)
		y(i) = native(i,1) * native(i,2);
	end

	u = linspace(x0(1), length(euler) * h, length(euler))';
	v = linspace(x0(2), length(euler) * h, length(euler))';
	for i=1:length(u)
		for j=1:length(v)
			z(i,j) = u(i) * v(j);
		end
	end
	
	hold on;
	mesh (u, v, z);
	plot3 (euler(:,1), euler(:,2), x, 'LineWidth', 2, 'Color', 'w');
	plot3 (native(:,1), native(:,2), y);
endfunction
