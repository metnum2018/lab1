function Error(x0, h, tol, maxIter)
	euler = EulerAdelante(x0(1), x0(2), x0(3), x0(4), h, tol, maxIter);
	for i=1:length(euler)
		x(i) = euler(i,1) * euler(i,2);
	end
	
	native = lsode("DiffEc", x0, t = linspace(0, length(euler) * h, length(euler)));
	for i=1:length(native)
		y(i) = native(i,1) * native(i,2);
	end
	
	subplot(2,1,1);
	plot(euler(:,1), abs(native(:,1)-euler(:,1)));
	subplot(2,1,2);
	plot(euler(:,2), abs(native(:,2)-euler(:,2)));
endfunction
