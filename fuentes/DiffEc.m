function ydot = DiffEc(x,t)
	u = x(1);
	v = x(2);
	p = x(3);
	q = x(4);
	
	ydot(1) = p;
	ydot(2) = q;
	ydot(3) = ((-2 * v)/(u^2 + v^2 + 1)) * p * q;
	ydot(4) = ((-2 * u)/(u^2 + v^2 + 1)) * p * q;
endfunction
